// 菜单栏

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 菜单栏查询数据库
async function selectCdl() {
    let sql = `SELECT * FROM menuBar`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



// ..........................................................................................
// 菜单栏删除
async function deleteCdl(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM menuBar WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 菜单栏修改
async function alterCdl(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE menuBar SET imgUrl='${opt.userpic}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}

// ..........................................................................................
// 菜单栏增加
async function upCdl(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO menuBar(imgUrl)VALUES(?)`;
        connection.query(sql, [opt.userpic], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}





// 修改查询语句单个id

async function selectXgcd(id) {
    let sql = ` SELECT * FROM menuBar WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

module.exports = {
    selectCdl,           // 菜单栏查询数据库
    deleteCdl,           // 菜单栏删除
    alterCdl,            // 菜单栏修改
    upCdl,               // 菜单栏增加
    selectXgcd,         // 点击修改显示的图片和本身的一样
}