// 家电

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 家电查询数据库
async function selectJd() {
    let sql = `SELECT * FROM homeAppliances`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

// ..........................................................................................
// 家电删除
async function deleteJd(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM homeAppliances WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 家电修改
async function alterJd(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE homeAppliances SET imgUrl='${opt.userpic}', phoneName='${opt.names}', descriptive='${opt.canss}', original='${opt.huods}', privilege='${opt.yuanjs}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0); 
            }
        });
    })
}

// ..........................................................................................
// 家电增加
async function upJd(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO homeAppliances(imgUrl,phoneName,descriptive, original, privilege)VALUES(?, ?, ?, ?, ?)`;
        connection.query(sql, [opt.userpic,opt.name,opt.cans, opt.huod, opt.yuanj], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}





// 修改查询语句单个id

async function selectXgjd(id) {
    let sql = ` SELECT * FROM homeAppliances WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

module.exports = {
    selectJd,           // 家电查询数据库
    deleteJd,           // 家电删除
    alterJd,            // 家电修改
    upJd,               // 家电增加
    selectXgjd,         // 点击修改显示的图片和本身的一样
}
