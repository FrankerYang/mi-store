// 广告

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 广告查询数据库
async function selectg() {
    let sql = `SELECT * FROM board`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



// ..........................................................................................
// 广告删除
async function deleteg(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM board WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 广告修改
async function alterg(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE board SET imgUrl='${opt.userpic}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}

// ..........................................................................................
// 广告增加
async function upg(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO board(imgUrl)VALUES(?)`;
        connection.query(sql, [opt.userpic], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}



// 修改查询语句单个id

async function selectXgg(id) {
    let sql = ` SELECT * FROM board WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



module.exports = {
    selectg,           // 广告查询数据库
    deleteg,           // 广告删除
    alterg,            // 广告修改
    upg,               // 广告增加
    selectXgg,          // 点击修改显示的图片和本身的一样
}