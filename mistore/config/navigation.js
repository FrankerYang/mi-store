// 主导航
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 主导航查询数据库
async function select() {
    let sql = `SELECT * FROM navigation`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            // console.log(result)
            // console.log(sql);
            if (err) {
                reject(err);
            } else {
                resolve(result);
                // console.log(result)

            }
        });
    })
}

// ..........................................................................................
// 主导航删除
async function deletenav(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM navigation WHERE id='${id}'`;
        
        // console.log(sql);
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 主导航修改
async function alternav(id, navName) {
    return new Promise((resolve, reject) => {
        // `UPDATE `navigation SET navName'='${opt.navName}' WHERE (id='${opt.id}')`
        let sql =  `UPDATE navigation SET navName='${navName}' WHERE (id='${id}')`
        // console.log(sql);
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 主导航增加
async function upnav(navName) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO navigation(navName)VALUES(?)`;
        // console.log(sql)
        connection.query(sql, [navName], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}



// 修改查询语句单个id

async function selectXgdh(id) {
    let sql = ` SELECT * FROM navigation WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

module.exports = {
    select,        // 主导航查询数据库
    deletenav,     // 主导航删除
    alternav,      // 主导航修改
    upnav,         // 主导航增加
    selectXgdh,    // 点击修改显示的图片和本身的一样
}