// 手机设置
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 手机设置查询数据库
async function selectSjj() {
    let sql = `SELECT * FROM capacity`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

// ..........................................................................................
// 手机设置删除
async function deleteSjj(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM capacity WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 手机设置修改
async function alterSjj(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE capacity SET imgUrl='${opt.userpic}', phoneName='${opt.names}', descriptive='${opt.canss}', original='${opt.huods}', privilege='${opt.yuanjs}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}

// ..........................................................................................
// 手机设置增加
async function upSjj(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO capacity(imgUrl,phoneName,descriptive, original, privilege)VALUES(?, ?, ?, ?, ?)`;
        connection.query(sql, [opt.userpic,opt.name,opt.cans, opt.huod, opt.yuanj], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}






// 修改查询语句单个id

async function selectXgss(id) {
    let sql = ` SELECT * FROM capacity WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

module.exports = {
    selectSjj,           // 手机设置查询数据库
    deleteSjj,           // 手机设置删除
    alterSjj,            // 手机设置修改
    upSjj,               // 手机设置增加
    selectXgss,         // 点击修改显示的图片和本身的一样
}
