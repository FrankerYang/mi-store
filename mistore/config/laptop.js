// 笔记本

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 笔记本查询数据库
async function selectBjb() {
    let sql = `SELECT * FROM laptop`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

// ..........................................................................................
// 笔记本删除
async function deleteBjb(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM laptop WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 笔记本修改
async function alterBjb(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE laptop SET imgUrl='${opt.userpic}', phoneName='${opt.names}', descriptive='${opt.canss}', original='${opt.huods}', privilege='${opt.yuanjs}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0); 
            }
        });
    })
}

// ..........................................................................................
// 笔记本增加
async function upBjb(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO laptop(imgUrl,phoneName,descriptive, original, privilege)VALUES(?, ?, ?, ?, ?)`;
        connection.query(sql, [opt.userpic,opt.name,opt.cans, opt.huod, opt.yuanj], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}






// 修改查询语句单个id

async function selectXgb(id) {
    let sql = ` SELECT * FROM laptop WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

module.exports = {
    selectBjb,           // 笔记本查询数据库
    deleteBjb,           // 笔记本删除
    alterBjb,            // 笔记本修改
    upBjb,               // 笔记本增加
    selectXgb,           // 点击修改显示的图片和本身的一样
}
