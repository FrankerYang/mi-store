// 轮播图

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 轮播图查询数据库
async function selectLb() {
    let sql = `SELECT * FROM slideshow`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



// ..........................................................................................
// 轮播图删除
async function deletelb(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM slideshow WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 轮播图修改
async function alterlb(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE slideshow SET imgUrl='${opt.userpic}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}

// ..........................................................................................
// 轮播图增加
async function uplb(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO slideshow(imgUrl)VALUES(?)`;
        connection.query(sql, [opt.userpic], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}



// 修改查询语句单个id

async function selectXglb(id) {
    let sql = ` SELECT * FROM slideshow WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



module.exports = {
    selectLb,           // 轮播图查询数据库
    deletelb,           // 轮播图删除
    alterlb,            // 轮播图修改
    uplb,               // 轮播图增加
    selectXglb,          // 点击修改显示的图片和本身的一样
}