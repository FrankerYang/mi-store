// 电视

const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '39.106.216.24',  // 服务器
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'mistore',   // 表名
});

// connection.connect(() => {
//     console.log(123)
// })

// 电视查询数据库
async function selectTV() {
    let sql = `SELECT * FROM TV`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}

// ..........................................................................................
// 电视删除
async function deleteTv(id) {
    return new Promise((resolve, reject) => {
        let sql = `DELETE FROM TV WHERE id='${id}'`;
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);
            }
        });
    })
}

// ..........................................................................................
// 电视修改
async function alterTv(opt) {
    return new Promise((resolve, reject) => {
        let sql =  `UPDATE TV SET imgUrl='${opt.userpic}', phoneName='${opt.names}', descriptive='${opt.canss}', original='${opt.huods}', privilege='${opt.yuanjs}' WHERE (id='${opt.id}')`
        connection.query(sql, (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0); 
            }
        });
    })
}

// ..........................................................................................
// 电视增加
async function upTv(opt) {
    return new Promise((resolve, reject) => {
        let sql = `INSERT INTO TV(imgUrl,phoneName,descriptive, original, privilege)VALUES(?, ?, ?, ?, ?)`;
        connection.query(sql, [opt.userpic,opt.name,opt.cans, opt.huod, opt.yuanj], (err, result) => {
            if (err) {
                reject(1);
            } else {
                resolve(0);

            }
        });
    })
}



// 修改查询语句单个id

async function selectXg(id) {
    let sql = ` SELECT * FROM TV WHERE id = '${id}'`;
    return new Promise((resolve, reject) => {
        connection.query(sql, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    })
}



module.exports = {
    selectTV,           // 电视查询数据库
    deleteTv,           // 电视删除
    alterTv,            // 电视修改
    upTv,               // 电视增加
    selectXg,          // 点击修改显示的图片和本身的一样
}