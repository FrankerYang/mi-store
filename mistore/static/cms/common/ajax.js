// function ajax(opt) {
//     let default_opt = {
//         url: '',
//         async: true,
//         method: 'GET',
//         data: {},
//         callback: null
//     }


//     let newOpt = Object.assign(default_opt, opt);

//     let xhr =  XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');

//     xhr.onreadystatechange = function () {
//         if (xhr.readyState === 4 && xhr.status === 200) {
//             newOpt.callback && newOpt.callback(xhr.responseText);
//         }
//     }

//     let search = '';
//     if (newOpt.method.toUpperCase() === 'GET') {
//         search = '?';
//         search += Object.keys(newOpt.data).map(key => key + '=' + newOpt.data[key]).join('&');
//     }

//     xhr.open(newOpt.method, newOpt.url + search, newOpt.async);

//     let data = null;

//     if (newOpt.method.toUpperCase() === 'POST') {
//         data = Object.keys(newOpt.data).map(key => key + '=' + newOpt.data[key]).join('&');
//     }

//     xhr.send(data);
// }




function ajax(opt) {
    let default_opt = {
        method: 'GET',
        url: '',
        data: {},
        async: true,
        callback: null
    };

    let newOpt = Object.assign(default_opt, opt);

    let xhr = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');

    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            newOpt.callback && newOpt.callback(xhr.responseText);
        }
    }

    let search = '';
    if (newOpt.method.toUpperCase() === 'GET') {
        search = '?';
        search += Object.keys(newOpt.data).map(key => key + '=' + newOpt.data[key]).join('&');
    }

    xhr.open(newOpt.method, newOpt.url + search, newOpt.async);

    let data = null;

    if (newOpt.method.toUpperCase() === 'POST') {
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        // data = Object.keys(newOpt.data).map(key => key + '=' + newOpt.data[key]).join('&');
        data = JSON.stringify(newOpt.data);
    }

    xhr.send(data);
}



//上传图片
function fPost(url, data, callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            callback && callback(xhr.responseText);
        }
    }
    xhr.open('POST', url);
    xhr.send(data);
}


// let xhr = new XMLHttpRequest();
// 第一步





