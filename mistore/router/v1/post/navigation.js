// 主导航
const Router = require('koa-router');

const router = new Router();

const { select, upnav, alternav, deletenav, selectXgdh } = require('../../../config/navigation');
    // 查询     添加    修改       删除
// 主导航查询
router.post('/v1/api/nav', async (ctx, next) => {
    // 查询的接口
    let result = await select();
    // console.log(result)
    ctx.body = result;

    next();
});

// 主导航删除的
router.post('/v1/api/deletenav', async (ctx) => {
    // 删除的接口
    let resultData = null;
    // console.log(ctx.request.body);
    let result = Object.keys(ctx.request.body);
        result.forEach(item => {
            resultData = JSON.parse(item).id
        })
        let resultD = await deletenav(resultData)
        // let resultD = await deletelb(resultData)
        // 主导航删除后重新查询数据库
        if (resultD === 0) {
            let result = await select();
            console.log(result)
            ctx.body = result;
        }
        // ctx.body = resultD;
});


// 主导航修改的
router.post('/v1/api/alternav', async ctx => {
    console.log(ctx.request.body);
    // 修改的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
        resultDataname = JSON.parse(item).navName
    })

    let resultD = await alternav(resultData, resultDataname)
    // 主导航修改后重新查询数据库
    if (resultD === 0) {
        let result = await select();
        console.log(result)
        ctx.body = result;
    }
    // ctx.body = resultD;
});

// 主导航增加的
router.post('/v1/api/upnav', async ctx => {
    // 增加的接口     
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).navName
    })

    let resultD = await upnav(resultData)
    // 主导航增加后重新查询数据库
    if (resultD === 0) {
        let result = await select();
                        // 查询
        console.log(result)
        ctx.body = result;
    }
    // ctx.body = resultD;
});


// 修改查询语句单个id的接口
router.post('/v1/api/selectXgdh', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXgdh(resultData);
    ctx.body = resultD;
    next();
});

module.exports = router;