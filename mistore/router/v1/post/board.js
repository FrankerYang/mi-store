// 广告
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fs = require('fs');
const path = require('path');
const { selectg, deleteg, alterg, upg, selectXgg } = require('../../../config/board');
        // 查询    删除      修改     增加
// 广告查询
router.post('/v1/api/selectg', async (ctx, next) => {
    // 查询的接口
    let result = await selectg();
    ctx.body = result;
    next();
});

// 广告删除的
router.post('/v1/api/deleteg', async (ctx) => {
    // 删除的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await deleteg(resultData)
    // 广告重新查询数据库
    if (resultD === 0) {
        let result = await selectg();  
        ctx.body = result;   
    }
});
// 广告修改
router.post('/v1/api/alterg', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let results = await alterg({
        userpic: 'http://39.106.216.24:3000' + picUrl,    
        id: ctx.request.body.id
    });
    // 广告修改后重新查询数据库
    if (results === 0) {
        let result = await selectg();
        ctx.body = result;
    }
})




// 广告增加的
// 图片上传
router.post('/v1/api/upg', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }

    let results = await upg({
        userpic: 'http://39.106.216.24:3000' + picUrl
    });
    // 广告上传图片后重新查询数据库
    if (results === 0) {
        let result = await selectg();
        ctx.body = result;
    }
})

// 修改查询语句单个id的接口
router.post('/v1/api/selectXgg', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXgg(resultData);
    ctx.body = resultD;
    next();
});

module.exports = router;