// 笔记本接口
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fs = require('fs');
const path = require('path');
const { selectBjb, deleteBjb, alterBjb, upBjb, selectXgb } = require('../../../config/laptop');
// 查询    删除      修改     增加
// 笔记本查询
router.post('/v1/api/selectBjb', async (ctx, next) => {
    // 查询的接口
    let result = await selectBjb();
    ctx.body = result;
    next();
});

// 笔记本删除的
router.post('/v1/api/deleteBjb', async (ctx) => {
    // 删除的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await deleteBjb(resultData)
    // 笔记本重新查询数据库
    if (resultD === 0) {
        let result = await selectBjb();
        ctx.body = result;
    }
});
// 笔记本修改
router.post('/v1/api/alterBjb', koaBody({
    
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
    
}), async ctx => {
    // console.log(ctx.request.files)
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files; 
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let lists1 = ctx.request.body;
    let results = await alterBjb({
        userpic: 'http://39.106.216.24:3000' + picUrl,
        id: ctx.request.body.id,
        names: lists1.names,
        canss: lists1.canss,
        huods: lists1.huods,
        yuanjs:lists1.yuanjs
    });
    // 笔记本修改后重新查询数据库
    if (results === 0) {
        let result = await selectBjb();
        ctx.body = result;
    }
})


// 返回格cms后台管理系统的图片的正确格式
// http://39.106.216.24:3000/ncms/images/17796a.jpg

// 笔记本增加的
// 图片上传
router.post('/v1/api/upBjb', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    // console.log(ctx.request.body)
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let list = ctx.request.body; // cms传输的数据传到数据库
    let results = await upBjb({
        userpic: 'http://39.106.216.24:3000' + picUrl,
        name: list.name,
        cans: list.cans,
        huod: list.huod,
        yuanj: list.yuanj
    })
    // 笔记本上传图片后重新查询数据库
    if (results === 0) {
        let result = await selectBjb();
        ctx.body = result;
    }
})





// 修改查询语句单个id的接口
router.post('/v1/api/selectXgb', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXgb(resultData);
    ctx.body = resultD;
    next();
});



module.exports = router;