// 轮播图
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fs = require('fs');
const path = require('path');
const { selectLb, deletelb, alterlb, uplb, selectXglb } = require('../../../config/slideshow');
        // 查询    删除      修改     增加
// 轮播图查询
router.post('/v1/api/selectLb', async (ctx, next) => {
    // 查询的接口
    let result = await selectLb();
    ctx.body = result;
    next();
});

// 轮播图删除的
router.post('/v1/api/deletelb', async (ctx) => {
    // 删除的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await deletelb(resultData)
    // 轮播图重新查询数据库
    if (resultD === 0) {
        let result = await selectLb();  
        ctx.body = result;   
    }
});
// 轮播图修改
router.post('/v1/api/alterlb', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let results = await alterlb({
        userpic: 'http://39.106.216.24:3000' + picUrl,    
        id: ctx.request.body.id
    });
    // 轮播图修改后重新查询数据库
    if (results === 0) {
        let result = await selectLb();
        ctx.body = result;
    }
})


// 返回格cms后台管理系统的图片的正确格式
// http://39.106.216.24:3000/ncms/images/17796a.jpg

// 轮播图增加的
// 图片上传
router.post('/v1/api/uplb', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }

    let results = await uplb({
        userpic: 'http://39.106.216.24:3000' + picUrl
    });
    // 轮播图上传图片后重新查询数据库
    if (results === 0) {
        let result = await selectLb();
        ctx.body = result;
    }
})



// 修改查询语句单个id的接口
router.post('/v1/api/selectXglb', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXglb(resultData);
    ctx.body = resultD;
    next();
});

module.exports = router;