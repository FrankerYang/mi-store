// 菜单栏接口
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fs = require('fs');
const path = require('path');
const { selectCdl, deleteCdl, alterCdl, upCdl, selectXgcd } = require('../../../config/menuBar');
        // 查询    删除      修改     增加
// 菜单栏查询
router.post('/v1/api/selectCdl', async (ctx, next) => {
    // 查询的接口
    let result = await selectCdl();
    ctx.body = result;
    next();
});

// 菜单栏删除的
router.post('/v1/api/deleteCdl', async (ctx) => {
    // 删除的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await deleteCdl(resultData)
    // 菜单栏重新查询数据库
    if (resultD === 0) {
        let result = await selectCdl();
        ctx.body = result;
    }
});
// 菜单栏修改
router.post('/v1/api/alterCdl', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let results = await alterCdl({
        userpic: 'http://39.106.216.24:3000' + picUrl,
        id: ctx.request.body.id
    });
    // 菜单栏修改后重新查询数据库
    if (results === 0) {
        let result = await selectCdl();
        ctx.body = result;
    }
})


// 返回格cms后台管理系统的图片的正确格式
// http://39.106.216.24:3000/ncms/images/17796a.jpg

// 菜单栏增加的
// 图片上传
router.post('/v1/api/upCdl', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }

    let results = await upCdl({
        userpic: 'http://39.106.216.24:3000' + picUrl
    })
    // 菜单栏上传图片后重新查询数据库
    if (results === 0) {
        let result = await selectCdl();
        ctx.body = result;
    }
})




// 修改查询语句单个id的接口
router.post('/v1/api/selectXgcd', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXgcd(resultData);
    ctx.body = resultD;
    next();
});







module.exports = router;