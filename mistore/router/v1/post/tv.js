// 电视接口
const Router = require('koa-router');
const router = new Router();
const koaBody = require('koa-body');
const fs = require('fs');
const path = require('path');
const { selectTV, deleteTv, alterTv, upTv, selectXg } = require('../../../config/tv');
// 查询    删除      修改     增加
// 电视查询
router.post('/v1/api/selectTV', async (ctx, next) => {
    // 查询的接口
    let result = await selectTV();
    ctx.body = result;
    next();
});

// 电视删除的
router.post('/v1/api/deleteTv', async (ctx) => {
    // 删除的接口
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await deleteTv(resultData)
    // 电视重新查询数据库
    if (resultD === 0) {
        let result = await selectTV();
        ctx.body = result;
    }
});
// 电视修改
router.post('/v1/api/alterTv', koaBody({
    
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
    
}), async ctx => {
    // console.log(ctx.request.files)
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files; 
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let lists1 = ctx.request.body;
    let results = await alterTv({
        // 判断的三目运算符
        // userpic:picUrl? ('http://39.106.216.24:3000' + picUrl):newpicUrl,

        picUrl:'http://39.106.216.24:3000' + picUrl,

        id: ctx.request.body.id,
        names: lists1.names,
        canss: lists1.canss,
        huods: lists1.huods,
        yuanjs:lists1.yuanjs
    });
    // 电视修改后重新查询数据库
    if (results === 0) {
        let result = await selectTV();
        ctx.body = result;
    }
})


// 返回格cms后台管理系统的图片的正确格式
// http://39.106.216.24:3000/ncms/images/17796a.jpg

// 电视增加的
// 图片上传
router.post('/v1/api/upTv', koaBody({
    multipart: true,
    formidable: {
        maxFieldsSize: 200 * 1024 * 1024
    }
}), async ctx => {
    // console.log(ctx.request.body)
    let picUrl = '';
    if (ctx.request.files.userpic) {
        let { userpic } = ctx.request.files;
        let rStream = fs.createReadStream(userpic.path);
        picUrl = '/images/' + Math.floor(Math.random() * 100000) + userpic.name;
        let str = path.join(process.cwd(), './static' + picUrl);
        const wStream = fs.createWriteStream(str);
        rStream.pipe(wStream);
    }
    let list = ctx.request.body; // cms传输的数据传到数据库
    let results = await upTv({
        userpic: 'http://39.106.216.24:3000' + picUrl,
        name: list.name,
        cans: list.cans,
        huod: list.huod,
        yuanj: list.yuanj
    })
    // 电视上传图片后重新查询数据库
    if (results === 0) {
        let result = await selectTV();
        ctx.body = result;
    }
})



// 修改查询语句单个id的接口
router.post('/v1/api/selectXg', async (ctx, next) => {
    let resultData = null;
    let result = Object.keys(ctx.request.body);
    result.forEach(item => {
        resultData = JSON.parse(item).id
    })
    let resultD = await selectXg(resultData);
    ctx.body = resultD;
    next();
});







module.exports = router;