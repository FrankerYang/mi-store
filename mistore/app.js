const Koa = require('koa');

const koaStatic = require('koa-static');

const bodyParser = require('koa-bodyparser');

const koaBody = require('koa-body');

const Router = require('koa-router');

const reDir = require('require-directory');

const koaViews = require('koa-views');

const path = require('path');

const app = new Koa();


//挂载解析参数
app.use(bodyParser());
//拼接路径
app.use(koaStatic(path.join(__dirname, './static')));

app.use(koaViews(path.join(__dirname, './static/cms/html'), {
    extension: 'html'
}));

// 解决跨域请求头
    app.use(async (ctx, next) => {
        ctx.set('Access-Control-Allow-Origin', '*');
        ctx.set('Access-Control-Allow-Credentials', true);
        await next();
        })

    //自动获取路由
    reDir(module, './router', {
        visit: function (modelR) {
            if (modelR instanceof Router) {
                app.use(modelR.routes()); 
            }
        }
    });


    
app.listen('3000', function () {
    console.log('listen 3000 数据库连接成功');
});